"""create tables

Revision ID: f7d99e3352f3
Revises: 
Create Date: 2021-01-31 12:07:57.467962

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'f7d99e3352f3'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('db_age_stars_score',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('age', sa.Integer(), nullable=False),
    sa.Column('score', sa.Float(), nullable=False),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('db_enjoy_stars_compare',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('name', sa.String(length=64), nullable=False),
    sa.Column('score', sa.String(length=64), nullable=False),
    sa.Column('time', sa.String(length=64), nullable=False),
    sa.Column('three_rate', sa.String(length=64), nullable=False),
    sa.Column('block', sa.String(length=64), nullable=False),
    sa.Column('assists', sa.String(length=64), nullable=False),
    sa.Column('backboard', sa.String(length=64), nullable=False),
    sa.Column('penalty', sa.String(length=64), nullable=False),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('db_height_stars_count',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('height_area', sa.String(length=64), nullable=False),
    sa.Column('count', sa.Integer(), nullable=False),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('db_pos_stars_count',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('pos', sa.String(length=64), nullable=False),
    sa.Column('count', sa.Integer(), nullable=False),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('db_team_stars_score',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('team', sa.String(length=64), nullable=False),
    sa.Column('score', sa.Float(), nullable=False),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('db_team_stars_score')
    op.drop_table('db_pos_stars_count')
    op.drop_table('db_height_stars_count')
    op.drop_table('db_enjoy_stars_compare')
    op.drop_table('db_age_stars_score')
    # ### end Alembic commands ###
