import re

from pymongo import MongoClient
import pandas as pd
import numpy as np
import pymysql

# 不同位置的球员数量占比
def pos_stars_count(df):
    # 处理位置的离散数据 中锋 中锋-前锋 前锋
    pos_series = df['位置'].str.split('-')
    pos_list = pos_series.tolist()
    # 将数据都放在一个数组中 再利用set方法的特性 去除重复数据 再转换成list数据
    pos_type_count = list(set([j for i in pos_list for j in i ]))
    # 构造0的矩阵
    zero_list = pd.DataFrame(np.zeros((df.shape[0],len(pos_type_count))),columns=pos_type_count)
    # 遍历0的矩阵 如果满足条件 则+1
    for i in range(len(zero_list)):
        zero_list.loc[i][pos_list[i]] = 1
    # 将每个列索引上的值累加
    pos_count = zero_list.sum().reset_index()
    # 构造列表嵌套列表的数据 方便后续mysql批量插入数据
    data = [[i['index'],int(i[0])] for i in pos_count.to_dict(orient="records")]
    print(data)
    return data

# 不同身高的球员数量占比
def height_stars_count(df):
    # 处理身高数据 193CM 去除CM
    df['身高'] = df['身高'].apply(lambda x:int(x.split('CM')[0]))
    # 对身高划分区间 分别为 180-190 190 190-200 200-210 210<
    # 将身高的数据类型转换成int
    df['身高'] = df['身高'].astype(np.int)
    # 首先添加身高区域列索引
    df['身高区间'] = ''
    # 通过循环操作 来划分区间
    for i in range(df.shape[0]):
        if df.loc[i]['身高']<190:
            df.loc[i,'身高区间'] = "<190"
        elif df.loc[i]['身高']>=190 and df.loc[i]['身高']<200:
            df.loc[i,'身高区间'] = "190-200"
        elif df.loc[i]['身高']>=200 and df.loc[i]['身高']<=210:
            df.loc[i,'身高区间'] = "200-210"
        elif df.loc[i]['身高']>210:
            df.loc[i,'身高区间'] = "210<"
    # 按照身高区域分组并进行统计
    grouped  = df.groupby('身高区间')['身高'].count().reset_index()
    # 构造列表嵌套列表的数据 方便后续mysql批量插入数据
    data = [[i['身高区间'],i['身高']] for i in grouped.to_dict(orient="records")]
    print(data)
    return data

# nba球员的年龄与场均得分的关系
def age_stars_count(df):
    # 数据中只有出生日期没有球员年龄 因此需要构造年龄数据
    # 将时间字符串转换成pandas时间类型
    df['出生日期'] = pd.to_datetime(df['出生日期'])
    # 再将pandas时间类型转换成DateTimeIndex类型
    date = pd.DatetimeIndex(df['出生日期'])
    # 取出具体的年 作为列索引填充数据
    df['year'] = date.year
    # 球员年龄 = 当前年份 - 出生年份
    df['年龄'] = df['year'].apply(lambda x:2021-int(x))
    # 构造列表嵌套元组的数据 方便后续mysql批量插入数据
    age = df['年龄'].sort_values().tolist()
    score = df['场均得分'].apply(lambda x:round(float(x),1)).tolist()
    data = list(zip(age,score))
    print(data)
    return data

# 统计不同球队的场均平均得分
def team_stars_score(df):
    # 按照球队进行分组 并统计每个球队球员的场均平均得分
    # 统计每队球员数量
    num_grouped = df.groupby('球队')['号码'].count()
    # 统计每队场均总得分
    # 由于 场均得分的数据为字符串数据 因此需要转换数据类型 进行累加
    df['场均得分'] = df['场均得分'].astype(np.float)
    score_grouped2 = df.groupby('球队')['场均得分'].sum()
    # 计算每个球队球员的场均平均得分
    avg_score = (score_grouped2/num_grouped).reset_index()
    # 对avg_score重新设置索引并修改列索引
    avg_score.columns = ['球队','球队场均平均得分']
    # 对球队场均平均得分数据进行处理 保留一位小数
    avg_score['球队场均平均得分'] = avg_score['球队场均平均得分'].apply(lambda x:round(x,1))
    # 构造列表嵌套列表的数据 方便后续mysql批量插入数据
    data = [[i['球队'],i['球队场均平均得分']] for i in avg_score.to_dict(orient="records")]
    print(data)
    return data

# 对比我最喜欢的三个nba球星数据 詹姆斯 库里 杜兰特
def enjoy_stars_compare(df):
    # 获取数据
    zms = df[df['姓名']=="勒布朗-詹姆斯"]
    kl = df[df['姓名']=="斯蒂芬-库里"]
    dlt = df[df['姓名']=="凯文-杜兰特"]
    # 拼接数据
    my_enjoy_stars = pd.concat([zms,kl,dlt],axis=0)
    # 比较参数 场均得分 场均上场时间 场均三分命中 场均盖帽 场均助攻 场均篮板 场均罚球率
    data = [[i['姓名'],i['场均得分'],i['场均上场时间'],i['场均三分命中'],i['场均盖帽'],i['场均助攻'],i['场均篮板'],i['场均罚球率']] for i in my_enjoy_stars.to_dict(orient="records")]
    print(data)
    return data

if __name__ == '__main__':
    # 初始化mongodb连接
    client = MongoClient()
    collection = client['test']['nba']
    # 获取数据
    nba_stars = collection.find({},{'_id':0})
    # 将数据转换成dataFrame类型
    df = pd.DataFrame(nba_stars)
    # 打印基本信息
    print(df.info())
    print(df.head(5))

    # 不同位置的球员数量占比
    # data = pos_stars_count(df)

    # 不同身高的球员数量占比
    # data =  height_stars_count(df)

    # nba球员的年龄与得分的关系
    # data = age_stars_count(df)

    # 统计不同球队的场均平均得分
    # data = team_stars_score(df)

    # 对比我最喜欢的三个nba球星数据 詹姆斯 库里 杜兰特
    data = enjoy_stars_compare(df)

    # 初始化mysql数据库连接
    # conn = pymysql.Connect(host="localhost",user="root",password="123456",port=3306,database="nba",charset="utf8")
    # with conn.cursor() as cursor:
    #     # 不同位置的球员数量占比
    #     # sql = "insert into db_pos_stars_count(pos,count) values(%s,%s)"
    #     # 不同身高的球员数量占比
    #     # sql = "insert into db_height_stars_count(height_area,count) values(%s,%s)"
    #     # nba球员的年龄与得分的关系
    #     # sql = "insert into db_age_stars_score(age,score) values(%s,%s)"
    #     # 统计不同球队的场均平均得分
    #     # sql = "insert into db_team_stars_score(team,score) values(%s,%s)"
    #     # 对比我最喜欢的三个nba球星数据 詹姆斯 库里 杜兰特
    #     sql = "insert into db_enjoy_stars_compare(name,score,time,three_rate,block,assists,backboard,penalty) values(%s,%s,%s,%s,%s,%s,%s,%s)"
    #     try:
    #         result = cursor.executemany(sql,data)
    #         if result:
    #             print('插入成功')
    #             conn.commit()
    #     except pymysql.MySQLError as err:
    #         print(err)
    #         conn.rollback()
    #     finally:
    #         conn.close()
    #
