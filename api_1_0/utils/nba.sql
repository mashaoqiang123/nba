/*
 Navicat Premium Data Transfer

 Source Server         : my_mysql
 Source Server Type    : MySQL
 Source Server Version : 50729
 Source Host           : localhost:3306
 Source Schema         : nba

 Target Server Type    : MySQL
 Target Server Version : 50729
 File Encoding         : 65001

 Date: 31/01/2021 21:21:56
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for alembic_version
-- ----------------------------
DROP TABLE IF EXISTS `alembic_version`;
CREATE TABLE `alembic_version`  (
  `version_num` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`version_num`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of alembic_version
-- ----------------------------
INSERT INTO `alembic_version` VALUES ('f7d99e3352f3');

-- ----------------------------
-- Table structure for db_age_stars_score
-- ----------------------------
DROP TABLE IF EXISTS `db_age_stars_score`;
CREATE TABLE `db_age_stars_score`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `age` int(11) NOT NULL,
  `score` float NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 201 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of db_age_stars_score
-- ----------------------------
INSERT INTO `db_age_stars_score` VALUES (1, 20, 34.7);
INSERT INTO `db_age_stars_score` VALUES (2, 20, 30.5);
INSERT INTO `db_age_stars_score` VALUES (3, 20, 28.8);
INSERT INTO `db_age_stars_score` VALUES (4, 20, 28.3);
INSERT INTO `db_age_stars_score` VALUES (5, 21, 27.7);
INSERT INTO `db_age_stars_score` VALUES (6, 21, 27.7);
INSERT INTO `db_age_stars_score` VALUES (7, 21, 27.5);
INSERT INTO `db_age_stars_score` VALUES (8, 21, 27.3);
INSERT INTO `db_age_stars_score` VALUES (9, 21, 27.1);
INSERT INTO `db_age_stars_score` VALUES (10, 21, 27);
INSERT INTO `db_age_stars_score` VALUES (11, 21, 27);
INSERT INTO `db_age_stars_score` VALUES (12, 21, 26.7);
INSERT INTO `db_age_stars_score` VALUES (13, 21, 26.5);
INSERT INTO `db_age_stars_score` VALUES (14, 22, 25.8);
INSERT INTO `db_age_stars_score` VALUES (15, 22, 25.7);
INSERT INTO `db_age_stars_score` VALUES (16, 22, 25.5);
INSERT INTO `db_age_stars_score` VALUES (17, 22, 24.6);
INSERT INTO `db_age_stars_score` VALUES (18, 22, 24.5);
INSERT INTO `db_age_stars_score` VALUES (19, 22, 24.1);
INSERT INTO `db_age_stars_score` VALUES (20, 22, 24);
INSERT INTO `db_age_stars_score` VALUES (21, 22, 23.8);
INSERT INTO `db_age_stars_score` VALUES (22, 22, 23.8);
INSERT INTO `db_age_stars_score` VALUES (23, 22, 23.4);
INSERT INTO `db_age_stars_score` VALUES (24, 22, 23.4);
INSERT INTO `db_age_stars_score` VALUES (25, 22, 23);
INSERT INTO `db_age_stars_score` VALUES (26, 23, 22.9);
INSERT INTO `db_age_stars_score` VALUES (27, 23, 22.9);
INSERT INTO `db_age_stars_score` VALUES (28, 23, 22.7);
INSERT INTO `db_age_stars_score` VALUES (29, 23, 22.6);
INSERT INTO `db_age_stars_score` VALUES (30, 23, 22.2);
INSERT INTO `db_age_stars_score` VALUES (31, 23, 22);
INSERT INTO `db_age_stars_score` VALUES (32, 23, 21.9);
INSERT INTO `db_age_stars_score` VALUES (33, 23, 21.9);
INSERT INTO `db_age_stars_score` VALUES (34, 23, 21.6);
INSERT INTO `db_age_stars_score` VALUES (35, 23, 20.8);
INSERT INTO `db_age_stars_score` VALUES (36, 23, 20.7);
INSERT INTO `db_age_stars_score` VALUES (37, 23, 20.5);
INSERT INTO `db_age_stars_score` VALUES (38, 23, 20.1);
INSERT INTO `db_age_stars_score` VALUES (39, 23, 20.1);
INSERT INTO `db_age_stars_score` VALUES (40, 23, 20);
INSERT INTO `db_age_stars_score` VALUES (41, 23, 20);
INSERT INTO `db_age_stars_score` VALUES (42, 23, 19.9);
INSERT INTO `db_age_stars_score` VALUES (43, 24, 19.3);
INSERT INTO `db_age_stars_score` VALUES (44, 24, 19.1);
INSERT INTO `db_age_stars_score` VALUES (45, 24, 18.9);
INSERT INTO `db_age_stars_score` VALUES (46, 24, 18.9);
INSERT INTO `db_age_stars_score` VALUES (47, 24, 18.9);
INSERT INTO `db_age_stars_score` VALUES (48, 24, 18.6);
INSERT INTO `db_age_stars_score` VALUES (49, 24, 18.5);
INSERT INTO `db_age_stars_score` VALUES (50, 24, 18.5);
INSERT INTO `db_age_stars_score` VALUES (51, 24, 18.1);
INSERT INTO `db_age_stars_score` VALUES (52, 24, 18.1);
INSERT INTO `db_age_stars_score` VALUES (53, 24, 17.9);
INSERT INTO `db_age_stars_score` VALUES (54, 24, 17.9);
INSERT INTO `db_age_stars_score` VALUES (55, 25, 17.9);
INSERT INTO `db_age_stars_score` VALUES (56, 25, 17.8);
INSERT INTO `db_age_stars_score` VALUES (57, 25, 17.7);
INSERT INTO `db_age_stars_score` VALUES (58, 25, 17.7);
INSERT INTO `db_age_stars_score` VALUES (59, 25, 17.2);
INSERT INTO `db_age_stars_score` VALUES (60, 25, 17.2);
INSERT INTO `db_age_stars_score` VALUES (61, 25, 17.2);
INSERT INTO `db_age_stars_score` VALUES (62, 25, 16.8);
INSERT INTO `db_age_stars_score` VALUES (63, 25, 16.8);
INSERT INTO `db_age_stars_score` VALUES (64, 25, 16.7);
INSERT INTO `db_age_stars_score` VALUES (65, 25, 16.7);
INSERT INTO `db_age_stars_score` VALUES (66, 25, 16.4);
INSERT INTO `db_age_stars_score` VALUES (67, 25, 16.1);
INSERT INTO `db_age_stars_score` VALUES (68, 25, 15.9);
INSERT INTO `db_age_stars_score` VALUES (69, 25, 15.9);
INSERT INTO `db_age_stars_score` VALUES (70, 25, 15.8);
INSERT INTO `db_age_stars_score` VALUES (71, 25, 15.5);
INSERT INTO `db_age_stars_score` VALUES (72, 25, 15.2);
INSERT INTO `db_age_stars_score` VALUES (73, 25, 15.2);
INSERT INTO `db_age_stars_score` VALUES (74, 26, 15.2);
INSERT INTO `db_age_stars_score` VALUES (75, 26, 15.2);
INSERT INTO `db_age_stars_score` VALUES (76, 26, 15);
INSERT INTO `db_age_stars_score` VALUES (77, 26, 14.9);
INSERT INTO `db_age_stars_score` VALUES (78, 26, 14.8);
INSERT INTO `db_age_stars_score` VALUES (79, 26, 14.6);
INSERT INTO `db_age_stars_score` VALUES (80, 26, 14.6);
INSERT INTO `db_age_stars_score` VALUES (81, 26, 14.6);
INSERT INTO `db_age_stars_score` VALUES (82, 26, 14.6);
INSERT INTO `db_age_stars_score` VALUES (83, 26, 14.5);
INSERT INTO `db_age_stars_score` VALUES (84, 26, 14.5);
INSERT INTO `db_age_stars_score` VALUES (85, 26, 14.4);
INSERT INTO `db_age_stars_score` VALUES (86, 26, 14.4);
INSERT INTO `db_age_stars_score` VALUES (87, 26, 14.3);
INSERT INTO `db_age_stars_score` VALUES (88, 26, 14.3);
INSERT INTO `db_age_stars_score` VALUES (89, 27, 14.3);
INSERT INTO `db_age_stars_score` VALUES (90, 27, 14.3);
INSERT INTO `db_age_stars_score` VALUES (91, 27, 14.3);
INSERT INTO `db_age_stars_score` VALUES (92, 27, 14.2);
INSERT INTO `db_age_stars_score` VALUES (93, 27, 13.9);
INSERT INTO `db_age_stars_score` VALUES (94, 27, 13.9);
INSERT INTO `db_age_stars_score` VALUES (95, 27, 13.9);
INSERT INTO `db_age_stars_score` VALUES (96, 27, 13.8);
INSERT INTO `db_age_stars_score` VALUES (97, 27, 13.7);
INSERT INTO `db_age_stars_score` VALUES (98, 27, 13.7);
INSERT INTO `db_age_stars_score` VALUES (99, 27, 13.6);
INSERT INTO `db_age_stars_score` VALUES (100, 27, 13.6);
INSERT INTO `db_age_stars_score` VALUES (101, 27, 13.6);
INSERT INTO `db_age_stars_score` VALUES (102, 27, 13.5);
INSERT INTO `db_age_stars_score` VALUES (103, 27, 13.4);
INSERT INTO `db_age_stars_score` VALUES (104, 27, 13.4);
INSERT INTO `db_age_stars_score` VALUES (105, 27, 13.2);
INSERT INTO `db_age_stars_score` VALUES (106, 28, 13.2);
INSERT INTO `db_age_stars_score` VALUES (107, 28, 13.1);
INSERT INTO `db_age_stars_score` VALUES (108, 28, 13.1);
INSERT INTO `db_age_stars_score` VALUES (109, 28, 13.1);
INSERT INTO `db_age_stars_score` VALUES (110, 28, 13);
INSERT INTO `db_age_stars_score` VALUES (111, 28, 13);
INSERT INTO `db_age_stars_score` VALUES (112, 28, 12.9);
INSERT INTO `db_age_stars_score` VALUES (113, 28, 12.9);
INSERT INTO `db_age_stars_score` VALUES (114, 28, 12.9);
INSERT INTO `db_age_stars_score` VALUES (115, 28, 12.9);
INSERT INTO `db_age_stars_score` VALUES (116, 28, 12.9);
INSERT INTO `db_age_stars_score` VALUES (117, 28, 12.8);
INSERT INTO `db_age_stars_score` VALUES (118, 28, 12.8);
INSERT INTO `db_age_stars_score` VALUES (119, 28, 12.7);
INSERT INTO `db_age_stars_score` VALUES (120, 28, 12.6);
INSERT INTO `db_age_stars_score` VALUES (121, 28, 12.5);
INSERT INTO `db_age_stars_score` VALUES (122, 28, 12.5);
INSERT INTO `db_age_stars_score` VALUES (123, 29, 12.4);
INSERT INTO `db_age_stars_score` VALUES (124, 29, 12.3);
INSERT INTO `db_age_stars_score` VALUES (125, 29, 12.3);
INSERT INTO `db_age_stars_score` VALUES (126, 29, 12.3);
INSERT INTO `db_age_stars_score` VALUES (127, 29, 12.2);
INSERT INTO `db_age_stars_score` VALUES (128, 29, 12.2);
INSERT INTO `db_age_stars_score` VALUES (129, 29, 12.1);
INSERT INTO `db_age_stars_score` VALUES (130, 29, 12.1);
INSERT INTO `db_age_stars_score` VALUES (131, 29, 12.1);
INSERT INTO `db_age_stars_score` VALUES (132, 29, 11.8);
INSERT INTO `db_age_stars_score` VALUES (133, 29, 11.8);
INSERT INTO `db_age_stars_score` VALUES (134, 29, 11.7);
INSERT INTO `db_age_stars_score` VALUES (135, 29, 11.7);
INSERT INTO `db_age_stars_score` VALUES (136, 29, 11.7);
INSERT INTO `db_age_stars_score` VALUES (137, 29, 11.6);
INSERT INTO `db_age_stars_score` VALUES (138, 29, 11.5);
INSERT INTO `db_age_stars_score` VALUES (139, 29, 11.4);
INSERT INTO `db_age_stars_score` VALUES (140, 29, 11.4);
INSERT INTO `db_age_stars_score` VALUES (141, 29, 11.4);
INSERT INTO `db_age_stars_score` VALUES (142, 29, 11.2);
INSERT INTO `db_age_stars_score` VALUES (143, 30, 11.2);
INSERT INTO `db_age_stars_score` VALUES (144, 30, 11.1);
INSERT INTO `db_age_stars_score` VALUES (145, 30, 11.1);
INSERT INTO `db_age_stars_score` VALUES (146, 30, 11);
INSERT INTO `db_age_stars_score` VALUES (147, 30, 10.9);
INSERT INTO `db_age_stars_score` VALUES (148, 30, 10.9);
INSERT INTO `db_age_stars_score` VALUES (149, 30, 10.9);
INSERT INTO `db_age_stars_score` VALUES (150, 30, 10.9);
INSERT INTO `db_age_stars_score` VALUES (151, 30, 10.9);
INSERT INTO `db_age_stars_score` VALUES (152, 31, 10.9);
INSERT INTO `db_age_stars_score` VALUES (153, 31, 10.7);
INSERT INTO `db_age_stars_score` VALUES (154, 31, 10.7);
INSERT INTO `db_age_stars_score` VALUES (155, 31, 10.7);
INSERT INTO `db_age_stars_score` VALUES (156, 31, 10.7);
INSERT INTO `db_age_stars_score` VALUES (157, 31, 10.6);
INSERT INTO `db_age_stars_score` VALUES (158, 31, 10.6);
INSERT INTO `db_age_stars_score` VALUES (159, 31, 10.6);
INSERT INTO `db_age_stars_score` VALUES (160, 31, 10.6);
INSERT INTO `db_age_stars_score` VALUES (161, 31, 10.5);
INSERT INTO `db_age_stars_score` VALUES (162, 31, 10.3);
INSERT INTO `db_age_stars_score` VALUES (163, 31, 10.3);
INSERT INTO `db_age_stars_score` VALUES (164, 31, 10.1);
INSERT INTO `db_age_stars_score` VALUES (165, 32, 10);
INSERT INTO `db_age_stars_score` VALUES (166, 32, 9.9);
INSERT INTO `db_age_stars_score` VALUES (167, 32, 9.9);
INSERT INTO `db_age_stars_score` VALUES (168, 32, 9.9);
INSERT INTO `db_age_stars_score` VALUES (169, 32, 9.9);
INSERT INTO `db_age_stars_score` VALUES (170, 32, 9.8);
INSERT INTO `db_age_stars_score` VALUES (171, 32, 9.8);
INSERT INTO `db_age_stars_score` VALUES (172, 32, 9.8);
INSERT INTO `db_age_stars_score` VALUES (173, 32, 9.8);
INSERT INTO `db_age_stars_score` VALUES (174, 33, 9.6);
INSERT INTO `db_age_stars_score` VALUES (175, 33, 9.6);
INSERT INTO `db_age_stars_score` VALUES (176, 33, 9.6);
INSERT INTO `db_age_stars_score` VALUES (177, 33, 9.5);
INSERT INTO `db_age_stars_score` VALUES (178, 33, 9.5);
INSERT INTO `db_age_stars_score` VALUES (179, 33, 9.5);
INSERT INTO `db_age_stars_score` VALUES (180, 33, 9.5);
INSERT INTO `db_age_stars_score` VALUES (181, 33, 9.4);
INSERT INTO `db_age_stars_score` VALUES (182, 33, 9.4);
INSERT INTO `db_age_stars_score` VALUES (183, 33, 9.4);
INSERT INTO `db_age_stars_score` VALUES (184, 33, 9.2);
INSERT INTO `db_age_stars_score` VALUES (185, 34, 9.2);
INSERT INTO `db_age_stars_score` VALUES (186, 34, 9.1);
INSERT INTO `db_age_stars_score` VALUES (187, 34, 9.1);
INSERT INTO `db_age_stars_score` VALUES (188, 34, 9.1);
INSERT INTO `db_age_stars_score` VALUES (189, 35, 9.1);
INSERT INTO `db_age_stars_score` VALUES (190, 35, 9);
INSERT INTO `db_age_stars_score` VALUES (191, 35, 9);
INSERT INTO `db_age_stars_score` VALUES (192, 35, 8.9);
INSERT INTO `db_age_stars_score` VALUES (193, 35, 8.8);
INSERT INTO `db_age_stars_score` VALUES (194, 35, 8.8);
INSERT INTO `db_age_stars_score` VALUES (195, 35, 8.6);
INSERT INTO `db_age_stars_score` VALUES (196, 36, 8.6);
INSERT INTO `db_age_stars_score` VALUES (197, 36, 8.6);
INSERT INTO `db_age_stars_score` VALUES (198, 36, 8.6);
INSERT INTO `db_age_stars_score` VALUES (199, 37, 8.6);
INSERT INTO `db_age_stars_score` VALUES (200, 37, 8.6);

-- ----------------------------
-- Table structure for db_enjoy_stars_compare
-- ----------------------------
DROP TABLE IF EXISTS `db_enjoy_stars_compare`;
CREATE TABLE `db_enjoy_stars_compare`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `score` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `three_rate` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `block` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `assists` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `backboard` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `penalty` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of db_enjoy_stars_compare
-- ----------------------------
INSERT INTO `db_enjoy_stars_compare` VALUES (1, '勒布朗-詹姆斯', '25.500', '33.100', '0.417', '0.400', '7.500', '7.800', '0.713');
INSERT INTO `db_enjoy_stars_compare` VALUES (2, '斯蒂芬-库里', '27.700', '33.700', '0.395', '0.100', '6.000', '5.400', '0.924');
INSERT INTO `db_enjoy_stars_compare` VALUES (3, '凯文-杜兰特', '30.500', '36.700', '0.444', '1.500', '5.500', '7.500', '0.860');

-- ----------------------------
-- Table structure for db_height_stars_count
-- ----------------------------
DROP TABLE IF EXISTS `db_height_stars_count`;
CREATE TABLE `db_height_stars_count`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `height_area` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `count` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of db_height_stars_count
-- ----------------------------
INSERT INTO `db_height_stars_count` VALUES (4, '190-200', 76);
INSERT INTO `db_height_stars_count` VALUES (5, '200-210', 92);
INSERT INTO `db_height_stars_count` VALUES (6, '210<', 7);
INSERT INTO `db_height_stars_count` VALUES (7, '<190', 25);

-- ----------------------------
-- Table structure for db_pos_stars_count
-- ----------------------------
DROP TABLE IF EXISTS `db_pos_stars_count`;
CREATE TABLE `db_pos_stars_count`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pos` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `count` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of db_pos_stars_count
-- ----------------------------
INSERT INTO `db_pos_stars_count` VALUES (4, '中锋', 40);
INSERT INTO `db_pos_stars_count` VALUES (5, '后卫', 113);
INSERT INTO `db_pos_stars_count` VALUES (6, '前锋', 106);

-- ----------------------------
-- Table structure for db_team_stars_score
-- ----------------------------
DROP TABLE IF EXISTS `db_team_stars_score`;
CREATE TABLE `db_team_stars_score`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `team` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `score` float NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 31 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of db_team_stars_score
-- ----------------------------
INSERT INTO `db_team_stars_score` VALUES (1, '76人', 15.5);
INSERT INTO `db_team_stars_score` VALUES (2, '公牛', 14.4);
INSERT INTO `db_team_stars_score` VALUES (3, '凯尔特人', 18.6);
INSERT INTO `db_team_stars_score` VALUES (4, '勇士', 16);
INSERT INTO `db_team_stars_score` VALUES (5, '国王', 15.2);
INSERT INTO `db_team_stars_score` VALUES (6, '太阳', 14.3);
INSERT INTO `db_team_stars_score` VALUES (7, '奇才', 16.8);
INSERT INTO `db_team_stars_score` VALUES (8, '尼克斯', 13.8);
INSERT INTO `db_team_stars_score` VALUES (9, '开拓者', 16.7);
INSERT INTO `db_team_stars_score` VALUES (10, '快船', 14.5);
INSERT INTO `db_team_stars_score` VALUES (11, '掘金', 14.4);
INSERT INTO `db_team_stars_score` VALUES (12, '森林狼', 17.5);
INSERT INTO `db_team_stars_score` VALUES (13, '步行者', 16.3);
INSERT INTO `db_team_stars_score` VALUES (14, '活塞', 13.4);
INSERT INTO `db_team_stars_score` VALUES (15, '湖人', 15.6);
INSERT INTO `db_team_stars_score` VALUES (16, '火箭', 14.1);
INSERT INTO `db_team_stars_score` VALUES (17, '灰熊', 13.6);
INSERT INTO `db_team_stars_score` VALUES (18, '热火', 13.7);
INSERT INTO `db_team_stars_score` VALUES (19, '爵士', 16.1);
INSERT INTO `db_team_stars_score` VALUES (20, '独行侠', 15.2);
INSERT INTO `db_team_stars_score` VALUES (21, '猛龙', 16.4);
INSERT INTO `db_team_stars_score` VALUES (22, '篮网', 21.1);
INSERT INTO `db_team_stars_score` VALUES (23, '老鹰', 14.8);
INSERT INTO `db_team_stars_score` VALUES (24, '雄鹿', 16.1);
INSERT INTO `db_team_stars_score` VALUES (25, '雷霆', 12.9);
INSERT INTO `db_team_stars_score` VALUES (26, '马刺', 13.9);
INSERT INTO `db_team_stars_score` VALUES (27, '骑士', 14.3);
INSERT INTO `db_team_stars_score` VALUES (28, '魔术', 14.7);
INSERT INTO `db_team_stars_score` VALUES (29, '鹈鹕', 15.7);
INSERT INTO `db_team_stars_score` VALUES (30, '黄蜂', 14.9);

SET FOREIGN_KEY_CHECKS = 1;
