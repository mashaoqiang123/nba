import re

from . import blue
from api_1_0.models import AgeStarsScore,enjoyStarsCompare,HeightStarsCount,PosStarsCount,TeamStarsScore
from flask import render_template
import pandas as pd
import numpy as np

# NBA不同位置的球员数量占比&NBA不同身高的球员数量占比
@blue.route('/drawPie')
def drawPie():
    pos_stars_count = PosStarsCount.query.all()
    height_stars_count = HeightStarsCount.query.all()
    # 将数据转换成列表嵌套字典类型数据 方便饼图数据渲染
    data1 = [{'name':i.pos,'value':i.count} for i in pos_stars_count]
    data2 = [{'name':i.height_area,'value':i.count} for i in height_stars_count]
    return render_template('drawPie.html',**locals())

# NBA球员的年龄与得分的关系散点图
@blue.route('/drawScatter')
def drawScatter():
    age_stars_score = AgeStarsScore.query.all()
    # 将数据转换成列表嵌套列表类型数据 方便散点图数据渲染
    data = [[i.age,i.score]  for i in age_stars_score]
    return render_template('drawScatter.html',**locals())

# NBA不同球队的场均平均得分柱状图
@blue.route('/drawBar')
def drawBar():
    team_stars_score = TeamStarsScore.query.all()
    # 将数据转换成两个列表类型数据 方便柱状图数据渲染
    team = [i.team for i in team_stars_score]
    score = [i.score for i in team_stars_score]
    print(team,score)
    return render_template('drawBar.html',**locals())

# 詹姆斯、库里、杜兰特NBA数据对比雷达图
@blue.route('/drawRadar')
def drawRadar():
    enjoy_stars_com = enjoyStarsCompare.query.all()
    data = [{'姓名':i.name,'场均得分':i.score,
             '场均上场时间':i.time,'场均盖帽':i.block,
             '场均助攻':i.assists,'场均篮板':i.backboard,'场均三分命中率':i.three_rate,'场均罚球率':i.penalty} for i in enjoy_stars_com]
    # 将数据转换成dataFrame类型
    df = pd.DataFrame(data,dtype=np.float)
    max_data = df.max()[1:]
    # 构造雷达图的indicator 将场均三分命中率 场均罚球率先排除出来 因为概率的最大值是为1 所以要另外处理
    # 为了让雷达图更加美观 将最大值在原有的基础上加上原有值的十分之一
    indicator = [{'name':k,'max':float(v)+float(v/10)} for k,v in max_data.to_dict().items() if not len(re.findall('率',k))>0]
    indicator.append({'name':'场均三分命中率','max':1})
    indicator.append({'name':'场均罚球率','max':1})
    print(df)
    # 分别去除三位球星的数据 不要姓名
    zms = df.loc[0][1:].tolist()
    kl = df.loc[1][1:].tolist()
    dlt = df.loc[2][1:].tolist()
    return render_template('drawRadar.html',**locals())