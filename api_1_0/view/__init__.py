from flask import Blueprint
# 让主程序在执行的时候知道数据库模型的存在
from api_1_0 import models

blue = Blueprint('show',__name__)

from . import show