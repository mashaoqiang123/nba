from api_1_0 import db

# 不同位置的球员数量模型
class PosStarsCount(db.Model):
    __tablename__ = "db_pos_stars_count"
    id = db.Column(db.Integer,primary_key=True,autoincrement=True)
    pos = db.Column(db.String(64),nullable=False)
    count = db.Column(db.Integer,nullable=False)


# 不同身高的球员数量占比
class HeightStarsCount(db.Model):
    __tablename__ = "db_height_stars_count"
    id = db.Column(db.Integer,primary_key=True,autoincrement=True)
    height_area = db.Column(db.String(64),nullable=False)
    count = db.Column(db.Integer,nullable=False)

# nba球员的年龄与场均得分的关系
class AgeStarsScore(db.Model):
    __tablename__ = "db_age_stars_score"
    id = db.Column(db.Integer,primary_key=True,autoincrement=True)
    age = db.Column(db.Integer,nullable=False)
    score = db.Column(db.Float,nullable=False)

# 统计不同球队的场均平均得分
class TeamStarsScore(db.Model):
    __tablename__ = "db_team_stars_score"
    id = db.Column(db.Integer,primary_key=True,autoincrement=True)
    team = db.Column(db.String(64),nullable=False)
    score = db.Column(db.Float,nullable=False)

# 对比我最喜欢的三个nba球星数据 詹姆斯 库里 杜兰特
class enjoyStarsCompare(db.Model):
    __tablename__ = "db_enjoy_stars_compare"
    id = db.Column(db.Integer,primary_key=True,autoincrement=True)
    name = db.Column(db.String(64),nullable=False)
    score = db.Column(db.String(64),nullable=False)
    time = db.Column(db.String(64),nullable=False)
    three_rate = db.Column(db.String(64),nullable=False)
    block = db.Column(db.String(64),nullable=False)
    assists  = db.Column(db.String(64),nullable=False)
    backboard = db.Column(db.String(64),nullable=False)
    penalty = db.Column(db.String(64),nullable=False)