from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from config import config_map
import pymysql
pymysql.install_as_MySQLdb()

db = SQLAlchemy()

def create_app(mode='develop'):
    # 创建app实例对象对象
    app = Flask(__name__)
    config_name = config_map[mode]
    print(config_name)
    app.config.from_object(config_name)

    # 加载数据库实例
    db.init_app(app)

    # 注册蓝图
    from api_1_0 import view
    app.register_blueprint(view.blue,url_prefix="/show")
    return app


